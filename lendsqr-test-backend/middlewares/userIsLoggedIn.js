const jwt = require("jsonwebtoken");
const db = require("../core/db");
module.exports = async (req, res, next) => {
  if (!req.headers) {
    return res.status(401).json({ message: "Unauthenticated" });
  }
  if (!req.headers["authorization"]) {
    return res.status(401).json({ message: "Unauthenticated" });
  }
  try {
    token = req.headers["authorization"];
    const payload = jwt.verify(token, process.env.SECRET_KEY);

    //check if user exists
    const user = await db("users").where({ id: payload.id }).first();
    if (!user) {
      // if user does not exist reject connection
      return res.status(401).json({ message: "User does not exist" });
    }
    delete user.password;
    req.user = user;
    next();
  } catch (err) {
    console.log(err);
    return res.status(401).json({ message: "Unauthenticated" });
  }
};
