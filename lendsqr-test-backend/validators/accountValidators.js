const { check, validationResult } = require("express-validator");
const db = require("../core/db");
const { decryptString } = require("../shared/utils");

module.exports = {
  fund: async (req, res, next) => {
    await check("amount")
      .notEmpty()
      .bail()
      .withMessage("Provide amount")
      .isNumeric()
      .bail()
      .withMessage("Must be a valid number")
      .custom(async (value) => {
        if (parseFloat(value) <= 0) {
          return Promise.reject("Must be greater than 0");
        }
      })
      .bail()
      .withMessage("Must be greater than 0")
      .run(req);

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
  withdraw: async (req, res, next) => {
    await check("amount")
      .notEmpty()
      .bail()
      .withMessage("Provide amount")
      .isNumeric()
      .bail()
      .withMessage("Must be a valid number")
      .custom(async (value) => {
        if (parseFloat(value) <= 0) {
          return Promise.reject("Must be greater than 0");
        }
      })
      .bail()
      .withMessage("Must be greater than 0")
      .custom(async (value) => {
        const account = await db("accounts")
          .where({ user_id: req.user.id })
          .first();
        let amount = parseFloat(decryptString(account.amount));
        if (value > amount) {
          return Promise.reject("You do not have up to that amount");
        }
      })
      .run(req);

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
  transfer: async (req, res, next) => {
    await check("email")
      .notEmpty()
      .bail()
      .withMessage("Please provide recipients email.")
      .isEmail()
      .bail()
      .withMessage("Provide a valid email.")
      .custom(async (value) => {
        if (value === req.user.email) {
          return Promise.reject("Cannot make a transfer to yourself.");
        }
      })
      .custom(async (value) => {
        //check if the account exists
        const user = await db("users").where({ email: value }).first();
        if (!user) {
          return Promise.reject("Invalid email address. Check and retry again");
        }
      })
      .run(req);

    //validate amount
    await check("amount")
      .notEmpty()
      .bail()
      .withMessage("Provide an amount")
      .isNumeric()
      .bail()
      .withMessage("Must be a number")
      .custom(async (value) => {
        if (value <= 0.0) {
          return Promise.reject("Must be greater than 0.00 naira");
        }
        const account = await db("accounts")
          .where({ user_id: req.user.id })
          .first();
        amount = Number.parseFloat(decryptString(account.amount));
        if (value > amount) {
          return Promise.reject("You don't have up to that amount");
        }
      })
      .run(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  },
};
