const { validationResult, check } = require("express-validator");

const requiredText = "Field is required";
const db = require("../core/db");

module.exports = {
  register: async (req, res, next) => {
    await check("email")
      .notEmpty()
      .withMessage(requiredText)
      .bail()
      .isEmail()
      .withMessage("Invalid email format")
      .bail()
      .custom(async (value) => {
        const [res] = await db("users")
          .count("id", { as: "c" })
          .where({ email: value });
        if (res.c) {
          return Promise.reject("A user with that email is already registered");
        }
      })
      .run(req);
    await check("firstName")
      .notEmpty()
      .bail()
      .withMessage(requiredText)
      .isString()
      .bail()
      .withMessage("Must be a string")
      .isLength({ max: 255 })
      .bail()
      .withMessage("Maximum of 255 characters")
      .run(req);
    await check("lastName")
      .notEmpty()
      .bail()
      .withMessage(requiredText)
      .isString()
      .bail()
      .withMessage("Must be a string")
      .isLength({ max: 255 })
      .bail()
      .withMessage("Maximum of 255 characters")
      .run(req);
    await check("password")
      .notEmpty()
      .bail()
      .withMessage(requiredText)
      .isString()
      .bail()
      .withMessage("Must be a string")
      .isLength({ min: 8, max: 50 })
      .bail()
      .withMessage("Provide a password between 8 and 50 characters")
      .run(req);

    const errors = validationResult(req);

    if (errors.isEmpty()) {
      next();
    } else {
      return res.status(422).json({ errors: errors.array() });
    }
  },
};
