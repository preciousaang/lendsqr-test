const bcrypt = require("bcrypt");
const crypto = require("crypto-js");
module.exports = {
  hashPassword: (password) => {
    return Promise.resolve(bcrypt.hash(password, 10));
  },
  verifyPassword: (password, hash) => {
    return Promise.resolve(bcrypt.compare(password, hash));
  },
  encryptString: (str) => {
    return crypto.AES.encrypt(str, process.env.SECRET_KEY).toString();
  },
  decryptString: (hash) => {
    return crypto.AES.decrypt(hash, process.env.SECRET_KEY).toString(
      crypto.enc.Utf8
    );
  },
};
