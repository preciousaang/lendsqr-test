const authService = require("../services/authService");
module.exports = {
  register: async (req, res) => {
    try {
      const { firstName, lastName, email, password } = req.body;
      const message = await authService.registerUser(
        firstName,
        lastName,
        email,
        password
      );
      return res.status(201).json({ message });
    } catch (e) {
      // Do some logging if you wish
      return res.status(400).json(e);
    }
  },
  login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const { token, user } = await authService.loginUser(email, password);
      return res.json({ token, user });
    } catch (e) {
      //do some logging if you want
      console.log(e);
      return res.status(400).json(e);
    }
  },
  user: async ({ user }, res) => {
    return res.json({ user });
  },
};
