const db = require("../core/db");
const {
  check,
  fund,
  withdraw,
  transfer,
} = require("../services/accountService");

module.exports = {
  check: async (req, res) => {
    try {
      const amount = await check(req.user.id);
      return res.json({ status: "successful", amount });
    } catch (e) {
      return res
        .status(400)
        .json({ message: "There was an error fetching you account" });
    }
  },
  fund: async (req, res) => {
    // return res.json("fund account");
    try {
      const amount = await fund(req.user.id, req.body.amount);
      return res.json({ status: "successful", amount });
    } catch (e) {
      return res
        .status(400)
        .json({ message: "There was an error funding your account" });
    }
  },
  withdraw: async (req, res) => {
    try {
      const balance = await withdraw(req.user.id, req.body.amount);
      return res.json({
        status: "successful",
        amount: req.body.amount,
        balance,
      });
    } catch (e) {
      return res
        .status(400)
        .json({ message: "There was an error withdrawing from your account" });
    }
  },

  transerToUser: async (req, res) => {
    try {
      const result = await transfer(
        req.body.email,
        req.body.amount,
        req.user.id
      );
      return res.json({
        status: "successful",
        amount: req.body.amount,
        balance: result,
      });
    } catch (e) {
      console.log(e);
      return res.status(400).json({ message: e });
    }
  },

  transactionHistory: async (req, res) => {
    try {
      const result = await db
        .select({
          tid: "t.id",
          suid: "t.sender_id",
          sFirstName: "sender.firstName",
          sLastName: "sender.lastName",
          sEmail: "sender.email",
          ruid: "t.recipient_id",
          rFirstName: "recipient.firstName",
          rLastName: "recipient.lastName",
          rEmail: "recipient.email",
          amount: "t.amount",
          date: "t.created_at",
        })
        .from({ t: "transactions" })
        .orderBy("t.created_at", "desc")
        .where({ sender_id: req.user.id })
        .orWhere({ recipient_id: req.user.id })
        .join({ sender: "users" }, function () {
          this.on("t.sender_id", "=", "sender.id");
        })
        .join({ recipient: "users" }, function () {
          this.on("t.recipient_id", "=", "recipient.id");
        });

      //remove their password
      result.map((res) => {
        delete res.password;
        if (res?.ruid === req.user.id) {
          //debit or credit
          res.doc = "credit";
        } else if (res?.suid === req.user.id) {
          res.doc = "debit";
        }
        return res;
      });

      return res.json({ transactions: result });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ message: err });
    }
  },
};
