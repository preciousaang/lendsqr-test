/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("accounts", function (table) {
    table.bigIncrements();
    table.bigInteger("user_id").unsigned();
    table
      .foreign("user_id")
      .references("id")
      .inTable("users")
      .onDelete("cascade");
    table.text("amount");
    table.timestamp("created_at", { precision: 6 }).defaultTo(knex.fn.now(6));
    table.timestamp("updated_at", { precision: 6 }).defaultTo(knex.fn.now(6));
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTableIfExists("accounts");
};
