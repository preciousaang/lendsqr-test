/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("transactions", (table) => {
    table.bigIncrements();
    table.bigInteger("sender_id").unsigned();
    table.bigInteger("recipient_id").unsigned().nullable();
    table.enum("type", ["transfer", "withdrawal", "funding"]);
    table.double("amount");
    table.timestamp("created_at", { precision: 6 }).defaultTo(knex.fn.now(6));
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTableIfExists("transactions");
};
