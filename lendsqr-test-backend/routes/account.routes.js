const userIsLoggedIn = require("../middlewares/userIsLoggedIn");

const accountController = require("../controllers/accountController");
const accountValidators = require("../validators/accountValidators");

const router = require("express").Router();

router.get("/check", userIsLoggedIn, accountController.check);

router.post(
  "/fund",
  userIsLoggedIn,
  accountValidators.fund,
  accountController.fund
);

router.post(
  "/withdraw",
  userIsLoggedIn,
  accountValidators.withdraw,
  accountController.withdraw
);

router.post(
  "/transfer",
  userIsLoggedIn,
  accountValidators.transfer,
  accountController.transerToUser
);

router.get(
  "/transactions",
  userIsLoggedIn,
  accountController.transactionHistory
);

module.exports = router;
