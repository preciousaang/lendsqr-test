const router = require("express").Router();
const authController = require("../controllers/authController");
const userIsLoggedIn = require("../middlewares/userIsLoggedIn");

const authValidators = require("../validators/authValidators");

router.post("/register", authValidators.register, authController.register);
router.post("/login", authController.login);
router.get("/user", userIsLoggedIn, authController.user);

module.exports = router;
