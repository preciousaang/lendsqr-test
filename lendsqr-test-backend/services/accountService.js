const db = require("../core/db");
const { decryptString, encryptString } = require("../shared/utils");

class AccountService {
  constructor() {
    this.check = this.check.bind(this);
    this.fund = this.fund.bind(this);
    this.transfer = this.transfer.bind(this);
  }
  async check(user_id) {
    try {
      let { amount } = await db("accounts").where({ user_id }).first();
      amount = parseFloat(decryptString(amount));
      return Promise.resolve(amount);
    } catch (e) {
      // console.log(e)
      return Promise.reject("There was error getting your account details");
    }
  }

  async fund(user_id, amount) {
    try {
      let userAmount = await this.check(user_id);
      userAmount += parseFloat(amount);
      await db("accounts")
        .where({ user_id })
        .first()
        .update({ amount: encryptString(userAmount.toString()) });

      //record transaction
      await db("transactions").insert({
        sender_id: user_id,
        amount: userAmount,
        type: "funding",
      });
      return Promise.resolve(userAmount);
    } catch (e) {
      // console.log(e)
      return Promise.reject("error funding account");
    }
  }

  async transfer(email, amount, user_id) {
    try {
      return await db.transaction(async (trx) => {
        //step 1. get reciepient account
        const recipientQuery = trx("accounts")
          .whereExists(function () {
            this.select("*")
              .from("users")
              .where({ email })
              .whereRaw("users.id=accounts.user_id");
          })
          .first();

        // step 2. get sender account
        const senderQuery = trx("accounts")
          .whereExists(function () {
            this.select("*")
              .from("users")
              .where({ id: user_id })
              .whereRaw("users.id=accounts.user_id");
          })
          .first();

        //step 3. carry out deduction and addition to sender and recipient account respectively

        const sender = await senderQuery;
        const recipient = await recipientQuery;

        sender.amount =
          parseFloat(decryptString(sender.amount)) - parseFloat(amount);
        recipient.amount =
          parseFloat(decryptString(recipient.amount)) + parseFloat(amount);

        // step 4. update records

        await recipientQuery.update({
          amount: encryptString(recipient.amount.toString()),
        });
        await senderQuery.update({
          amount: encryptString(sender.amount.toString()),
        });

        //step 5. record transaction

        await trx("transactions").insert({
          sender_id: sender.id,
          recipient_id: recipient.id,
          type: "transfer",
          amount,
        });

        //exit method
        return Promise.resolve(sender.amount);
      });
    } catch (e) {
      // console.log(e);
      console.log(e);
      return Promise.reject("Error doing transfer");
    }
  }

  async withdraw(user_id, amount) {
    try {
      return db.transaction(async (trx) => {
        // step 1. get user account
        const accountQuery = trx("accounts").where({ user_id }).first();
        const account = await accountQuery;
        // step 2. deduct the amount rom account balance
        account.amount = parseFloat(decryptString(account.amount)) - amount;

        // step 3. update  records
        await accountQuery.update({
          amount: encryptString(account.amount.toString()),
        });

        // step 4. record transactoins

        await trx("transactions").insert({
          type: "withdrawal",
          amount,
          sender_id: user_id,
        });

        // exit method
        return Promise.resolve(account.amount);
      });
    } catch (err) {
      console.log(err);
      return Promise.reject("There was an error in withdrawal");
    }
  }
}

const account = new AccountService();

module.exports = account;
