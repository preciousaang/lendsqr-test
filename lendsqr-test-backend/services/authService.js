const {
  hashPassword,
  verifyPassword,
  encryptString,
} = require("../shared/utils");
const db = require("../core/db");
const jwt = require("jsonwebtoken");
module.exports = {
  registerUser: async (firstName, lastName, email, password) => {
    try {
      await db.transaction(async (trx) => {
        const user_id = await db("users")
          .insert({
            firstName,
            lastName,
            email,
            password: await hashPassword(password),
          })
          .transacting(trx);
        await db("accounts")
          .insert({ user_id, amount: encryptString("0.00") })
          .transacting(trx);
      });
      return Promise.resolve("User created");
    } catch (e) {
      return Promise.reject("Error creating user account");
    }
  },
  loginUser: async (email, password) => {
    const user = await db("users").where({ email: email }).first();

    if (!user) {
      return Promise.reject({ email: "No user with this email exists" });
    }
    const isValid = await verifyPassword(password, user.password);
    if (!isValid) {
      return Promise.reject({ password: "Invalid password" });
    }

    //Login User
    const token = jwt.sign({ id: user.id }, process.env.SECRET_KEY, {
      expiresIn: "1 year",
    });
    //REMOVE PASSWORD FROM OBJECT
    delete user.password;
    return Promise.resolve({ token, user });
  },
};
