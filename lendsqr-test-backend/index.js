require("dotenv").config();
const express = require("express");
const helmet = require("helmet");
const app = express();
// app.use(helmet());
const cors = require("cors");
app.use(cors());
const PORT = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const authRoutes = require("./routes/auth.routes");

const accountRoutes = require("./routes/account.routes");

app.use("/api/auth", authRoutes);
app.use("/api/account", accountRoutes);

app.use((req, res) => {
  return res.status(404).json("Not found");
});

app.listen(PORT, () => {
  console.log("Listening on port: " + PORT);
});
