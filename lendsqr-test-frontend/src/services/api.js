import axios from "axios";

export default axios.create({
  baseURL: process.env.REACT_APP_API_URL || 4000,
  headers: {
    Authorization: localStorage.getItem("token"),
  },
});
