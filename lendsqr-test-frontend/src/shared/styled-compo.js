import styled from "styled-components";
import { Link } from "react-router-dom";
import { Field } from "formik";

export const Wrapper = styled.div`
  width: 70%;
  border: 1px solid #d2dadd;
  margin: 10px auto;
  padding: 15px;
  border-radius: 10px;
`;

export const MainTitle = styled.h2`
  font-size: 120%;
  text-align: center;
  font-weight: bold;
  margin-bottom: 3px;
  color: #3796bf;
`;

export const Subtitle = styled.h3`
  font-size: 100%;
`;

export const FormControl = styled.div`
  display: block;
  margin: 1rem 0;
`;

export const ErrorWrapper = styled.p`
  color: red;
  font-size: 80%;
`;

export const ActionGroups = styled.div`
  display: flex;
  justify-content: space-evenly;
`;

export const ActionButton = styled(Link)`
  text-decoration: none;
  background-color: #60a8c9;
  padding: 5px;
  color: white;
  border-radius: 2px;
  &:hover {
    background-color: #64a7c4;
  }
`;

export const DangerButton = styled.button`
  text-decoration: none;
  background-color: #f49292;
  padding: 5px;
  color: white;
  border-radius: 2px;
  border: 0;
  cursor: pointer;
  &:hover {
    background-color: #f29898;
  }
`;

export const BalanceWidget = styled.div`
  padding: 10px;
  background-color: #f7c560;
  margin: 20px 0;
  color: white;
  border-radius: 5px;
`;

export const InputField = styled(Field)`
  display: block;
  outline: none;
  margin: 2px;
  height: 20px;
  font-size: 20px;
  color: #4c4d4f;
  padding: 3px;
  border-radius: 2px;
  border: 1.5px solid #d2dadd;
`;

export const FormButton = styled.button`
  text-decoration: none;
  background-color: #60a8c9;
  padding: 5px;
  color: white;
  border-radius: 2px;
  cursor: pointer;
  border: 0;
  &:hover {
    background-color: #64a7c4;
  }
  &:disabled {
    background-color: #a0d0e5;
    cursor: auto;
  }
  font-size: 110%;
`;

export const BackLink = styled(Link)`
  text-decoration: none;
  color: #f49292;
`;
