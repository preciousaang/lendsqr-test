import * as Yup from "yup";

export const requiredText = "Required Field!";
export const tooLongText = "Too Long!";

export const loginSchema = Yup.object().shape({
  email: Yup.string().email("Must be valid email").required(requiredText),
  password: Yup.string().required(requiredText),
});

export const registerSchema = Yup.object().shape({
  firstName: Yup.string().max(50, tooLongText).required(requiredText),
  lastName: Yup.string().max(50, tooLongText).required(requiredText),
  email: Yup.string().required(requiredText).email("Enter valid email"),
  password: Yup.string().required(requiredText).max(50, tooLongText),
});

export const fundingSchema = Yup.object().shape({
  amount: Yup.number()
    .typeError("Must be a number")
    .positive("Cannot have a negative number ")
    .min(1, "Minimum of 1")
    .required(requiredText),
});

export const transferSchema = Yup.object().shape({
  amount: Yup.number()
    .typeError("Must be a number")
    .positive("Cannot have a negative number ")
    .min(1, "Minimum of 1")
    .required(requiredText),
  email: Yup.string().email("Must be a valid email").required(requiredText),
});
