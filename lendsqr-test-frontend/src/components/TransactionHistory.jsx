import React from "react";
import { useUserAuth } from "../contexts/AuthContext";
import moment from "moment";

function TransactionHistory() {
  const { transactions } = useUserAuth();
  return (
    <>
      <ul>
        {transactions.map((t) => (
          <li key={t.tid}>
            {t.doc === "credit"
              ? "You recieved ₦" +
                t.amount +
                " from " +
                t.sFirstName +
                " " +
                t.sLastName
              : "You sent ₦" +
                t.amount +
                " to " +
                t.rFirstName +
                " " +
                t.rLastName}{" "}
            - {moment(t.date).format("lll")}
          </li>
        ))}
      </ul>
    </>
  );
}

export default TransactionHistory;
