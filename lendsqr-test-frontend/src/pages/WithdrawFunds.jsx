import { Form, Formik } from "formik";
import React from "react";

import { useUserAuth } from "../contexts/AuthContext";
import {
  BackLink,
  ErrorWrapper,
  FormButton,
  FormControl,
  InputField,
  MainTitle,
  Wrapper,
} from "../shared/styled-compo";
import { converApiErrors } from "../shared/utils";
import { fundingSchema } from "../shared/validationSchemas";

function WithdrawFunds() {
  const auth = useUserAuth();
  const onWithdraw = (values, actions) => {
    if (window.confirm("Are you sure?")) {
      auth
        .withdrawFunds(values.amount)
        .then((res) => {
          actions.resetForm();
          alert("Withdrawal successful");
        })
        .catch((err) => {
          if (err.response.status === 422) {
            actions.setErrors(converApiErrors(err.response.data.errors));
          }
        })
        .finally(() => {
          actions.setSubmitting(false);
        });
    }
    actions.setSubmitting(false);
  };
  return (
    <Wrapper>
      <BackLink to="/dashboard"> &laquo; Back to Dashboard</BackLink>
      <MainTitle>Widthdraw Funds </MainTitle>
      <Formik
        onSubmit={onWithdraw}
        initialValues={{ amount: "" }}
        validationSchema={fundingSchema}
      >
        {({ errors, touched, isSubmitting, isValid }) => (
          <Form>
            <FormControl>
              <label htmlFor="amount">Amount</label>
              <InputField
                name="amount"
                id="amount"
                placeholder="Enter amount to withdraw"
              />
              {errors.amount && touched.amount && (
                <ErrorWrapper>{errors.amount}</ErrorWrapper>
              )}
            </FormControl>

            <FormButton disabled={!isValid || isSubmitting} type="submit">
              Withdraw
            </FormButton>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
}

export default WithdrawFunds;
