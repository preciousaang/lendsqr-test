import React from "react";
import { Formik, Field, Form } from "formik";
import {
  Wrapper,
  FormControl,
  ErrorWrapper,
  MainTitle,
} from "../shared/styled-compo";
import { registerSchema } from "../shared/validationSchemas";

function Register() {
  const onRegister = (value, actions) => {
    console.log(value);
    actions.setSubmitting(false);
  };
  return (
    <Wrapper>
      <MainTitle>Register</MainTitle>
      <Formik
        initialValues={{ firstName: "", lastName: "", email: "", password: "" }}
        validationSchema={registerSchema}
        onSubmit={onRegister}
      >
        {({ errors, touched, isValid, isSubmitting }) => (
          <Form>
            <FormControl>
              <label htmlFor="firstName">First Name</label>
              <Field
                type="text"
                name="firstName"
                id="firstName"
                placeholder="Enter First Name"
              />
              {errors.firstName && touched.firstName && (
                <ErrorWrapper>{errors.firstName}</ErrorWrapper>
              )}
            </FormControl>
            <FormControl>
              <label htmlFor="lastName">Last Name</label>
              <Field
                type="text"
                name="lastName"
                id="lastName"
                placeholder="Enter Last Name"
              />
              {errors.lastName && touched.lastName && (
                <ErrorWrapper>{errors.lastName}</ErrorWrapper>
              )}
            </FormControl>

            <FormControl>
              <label htmlFor="email">Email</label>
              <Field name="email" id="email" placeholder="Enter Email" />
              {errors.email && touched.email && (
                <ErrorWrapper>{errors.email}</ErrorWrapper>
              )}
            </FormControl>
            <FormControl>
              <label htmlFor="password">Password</label>
              <Field
                type="password"
                name="password"
                id="password"
                placeholder="*****"
              />
              {errors.password && touched.password && (
                <ErrorWrapper>{errors.password}</ErrorWrapper>
              )}
            </FormControl>
            <button type="submit" disabled={!isValid || isSubmitting}>
              Register
            </button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
}

export default Register;
