import React from "react";
import {
  ActionButton,
  ActionGroups,
  MainTitle,
  Wrapper,
} from "../shared/styled-compo";

function Home() {
  return (
    <Wrapper>
      <MainTitle>Welcome to the bank</MainTitle>
      <br />
      <ActionGroups>
        <ActionButton to="/login">Login</ActionButton>
        <ActionButton to="/register">Register</ActionButton>
      </ActionGroups>
    </Wrapper>
  );
}

export default Home;
