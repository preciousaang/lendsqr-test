import React from "react";
import { Form, Formik } from "formik";
import {
  BackLink,
  ErrorWrapper,
  FormButton,
  FormControl,
  InputField,
  MainTitle,
  Wrapper,
} from "../shared/styled-compo";
import { fundingSchema } from "../shared/validationSchemas";

import { converApiErrors } from "../shared/utils";
import { useUserAuth } from "../contexts/AuthContext";

function FundAccount() {
  const auth = useUserAuth();
  const onFundAccount = (values, actions) => {
    if (window.confirm("Are you sure")) {
      auth
        .fundAccount(values.amount)
        .then((res) => {
          actions.resetForm();
          alert("Funding successful");
        })
        .catch((err) => {
          actions.setErrors(converApiErrors(err.response.data.errors));
        })
        .finally(() => {
          actions.setSubmitting(false);
        });
    }
    actions.setSubmitting(false);
  };
  return (
    <Wrapper>
      <BackLink to="/dashboard"> &laquo; Back to Dashboard</BackLink>
      <MainTitle>Fund Account</MainTitle>
      <Formik
        onSubmit={onFundAccount}
        initialValues={{ amount: "" }}
        validationSchema={fundingSchema}
      >
        {({ isSubmitting, isValid, errors }) => (
          <Form>
            <FormControl>
              <label htmlFor="amount">Amount</label>
              <InputField name="amount" id="amount" />
              {errors.amount && <ErrorWrapper>{errors.amount}</ErrorWrapper>}
            </FormControl>
            <FormButton type="submit" disabled={!isValid || isSubmitting}>
              Fund Account
            </FormButton>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
}

export default FundAccount;
