import React from "react";
import {
  Wrapper,
  FormControl,
  ErrorWrapper,
  MainTitle,
  InputField,
  FormButton,
  BackLink,
} from "../shared/styled-compo";
import { Formik, Form } from "formik";
import { transferSchema } from "../shared/validationSchemas";
import { useUserAuth } from "../contexts/AuthContext";
import { converApiErrors } from "../shared/utils";

function TransferFunds() {
  const auth = useUserAuth();
  const onTransferFunds = (values, actions) => {
    if (window.confirm("Are you sure")) {
      auth
        .transferFunds(values.email, values.amount)
        .then((res) => {
          alert("Tranfer of funds completed");
          actions.resetForm();
        })
        .catch((err) => {
          console.log(err.response);
          actions.setErrors(converApiErrors(err.response.data.errors));
        })
        .finally(() => {
          actions.setSubmitting(false);
        });
    }
    actions.setSubmitting(false);
  };
  return (
    <Wrapper>
      <BackLink to="/dashboard"> &laquo; Back to Dashboard</BackLink>
      <MainTitle>Transfer Funds</MainTitle>
      <Formik
        onSubmit={onTransferFunds}
        initialValues={{ amount: "", email: "" }}
        validationSchema={transferSchema}
      >
        {({ isSubmitting, isValid, errors, touched }) => (
          <Form>
            <FormControl>
              <label htmlFor="email">Recipient Email</label>
              <InputField name="email" id="email" />
              {errors.email && touched.email && (
                <ErrorWrapper>{errors.email}</ErrorWrapper>
              )}
            </FormControl>
            <FormControl>
              <label htmlFor="amount">Amount</label>
              <InputField name="amount" id="amount" />
              {errors.amount && touched.amount && (
                <ErrorWrapper>{errors.amount}</ErrorWrapper>
              )}
            </FormControl>
            <FormButton type="submit" disabled={!isValid || isSubmitting}>
              Transfer Funds
            </FormButton>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
}

export default TransferFunds;
