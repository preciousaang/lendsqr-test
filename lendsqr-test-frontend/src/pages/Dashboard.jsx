import React from "react";
import { useHistory } from "react-router-dom";
import TransactionHistory from "../components/TransactionHistory";
import { useUserAuth } from "../contexts/AuthContext";
import {
  MainTitle,
  Wrapper,
  Subtitle,
  ActionGroups,
  ActionButton,
  BalanceWidget,
  DangerButton,
} from "../shared/styled-compo";

function Dashboard() {
  const { balance, user, logout } = useUserAuth();
  const history = useHistory();
  return (
    <Wrapper>
      <MainTitle>Welcome, {user?.firstName}</MainTitle>
      <BalanceWidget>
        Your balance: <Subtitle>&#8358;{balance}</Subtitle>
      </BalanceWidget>

      <ActionGroups>
        <ActionButton to="/fund-account"> Fund Account</ActionButton>
        <ActionButton to="/transfer-funds">Transfer Funds</ActionButton>
        <ActionButton to="/withdraw-funds">Withdraw Funds</ActionButton>
        <DangerButton
          type="button"
          onClick={() => {
            if (window.confirm("Are you sure?")) {
              logout();
              history.replace("/login");
            }
          }}
        >
          Logout
        </DangerButton>
      </ActionGroups>
      <br />
      <hr style={{ margin: "5px 0" }} />
      <TransactionHistory />
    </Wrapper>
  );
}

export default Dashboard;
