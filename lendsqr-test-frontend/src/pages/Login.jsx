import React from "react";
import {
  ErrorWrapper,
  FormButton,
  FormControl,
  InputField,
  MainTitle,
  Wrapper,
} from "../shared/styled-compo";
import { Formik, Form } from "formik";
import { loginSchema } from "../shared/validationSchemas";
import { useUserAuth } from "../contexts/AuthContext";

function Login() {
  const auth = useUserAuth();

  const onLogin = async (values, { setSubmitting, setFieldError }) => {
    try {
      await auth.login(values.email, values.password);
      window.location = "/dashboard";
      setSubmitting(false);
    } catch (err) {
      if (err.response.status === 400) {
        const errors = err.response.data;

        for (const error in errors) {
          setFieldError(error, errors[error]);
        }
        setSubmitting(false);
      }
    }
  };
  return (
    <Wrapper>
      <MainTitle>Login</MainTitle>
      <Formik
        validationSchema={loginSchema}
        onSubmit={onLogin}
        initialValues={{ email: "", password: "" }}
      >
        {({ errors, isValid, isSubmitting, touched }) => (
          <Form>
            <FormControl>
              <label htmlFor="email">Email</label>
              <InputField name="email" id="email" />
              {errors.email && touched.email && (
                <ErrorWrapper>{errors.email}</ErrorWrapper>
              )}
            </FormControl>
            <FormControl>
              <label htmlFor="password">Password</label>
              <InputField name="password" type="password" id="password" />
              {errors.password && touched.password && (
                <ErrorWrapper>{errors.password}</ErrorWrapper>
              )}
            </FormControl>
            <FormButton type="submit" disabled={!isValid || isSubmitting}>
              Login
            </FormButton>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
}

export default Login;
