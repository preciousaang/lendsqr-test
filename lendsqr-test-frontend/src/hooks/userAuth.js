import { useEffect, useState } from "react";
import axios from "../services/api";

const useAuth = () => {
  const [token, setToken] = useState(localStorage.getItem("token"));
  // const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(null);
  const [balance, setBalance] = useState(0.0);
  const [transactions, setTransactions] = useState([]);

  const getUser = () => {
    // setLoading(true);
    axios
      .get("/auth/user")
      .then((res) => {
        setUser(res.data.user);
      })
      .then(async () => {
        await checkBalance();
        await getTransactionHistory();
      })
      .catch((err) => {
        setUser(null);
        setToken(null);
        localStorage.removeItem("token");
      })
      .finally(() => {
        // setLoading(false);
      });
  };
  useEffect(() => {
    (() => {
      getUser();
    })();
  }, []);

  const login = async (email, password) => {
    try {
      const res = await axios.post("/auth/login", { email, password });

      localStorage.setItem("token", res.data.token);
      setUser(res.data.user);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  const logout = () => {
    setToken(null);
    setUser(null);
    setBalance(0.0);
    localStorage.removeItem("token");
  };

  const withdrawFunds = async (amount) => {
    try {
      const res = await axios.post("/account/withdraw", { amount });
      setBalance(res.data.balance);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  const transferFunds = async (email, amount) => {
    try {
      const res = await axios.post("/account/transfer", { email, amount });
      setBalance(res.data.balance);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  const checkBalance = async () => {
    try {
      const res = await axios.get("/account/check");
      setBalance(res.data.amount);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  const fundAccount = async (amount) => {
    try {
      const res = await axios.post("account/fund", { amount });
      setBalance(res.data.amount);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  const getTransactionHistory = async () => {
    try {
      const res = await axios.get("/account/transactions");
      setTransactions(res.data.transactions);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  return {
    token,
    user,
    balance,
    transactions,
    login,
    logout,
    checkBalance,
    transferFunds,
    withdrawFunds,
    fundAccount,
  };
};

export default useAuth;
