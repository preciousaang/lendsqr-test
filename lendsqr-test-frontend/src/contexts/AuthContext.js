import { createContext, useContext } from "react";
import useAuth from "../hooks/userAuth";
const authContext = createContext(null);

const ProvideUserAuth = ({ children }) => {
  const auth = useAuth();

  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
};

const useUserAuth = () => useContext(authContext);

export { ProvideUserAuth, useUserAuth };
