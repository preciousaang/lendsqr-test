import { lazy, Suspense, useEffect } from "react";
import "./App.css";
import { Switch, Route, useHistory } from "react-router-dom";

const Register = lazy(() => import("./pages/Register"));
const Login = lazy(() => import("./pages/Login"));
const Home = lazy(() => import("./pages/Home"));
const PageNotFound = lazy(() => import("./pages/PageNotFound"));
const FundAccount = lazy(() => import("./pages/FundAccount"));
const Dashboard = lazy(() => import("./pages/Dashboard"));
const WithdrawFunds = lazy(() => import("./pages/WithdrawFunds"));
const TransferFunds = lazy(() => import("./pages/TransferFunds"));

function App() {
  return (
    <Suspense fallback={<>Loading</>}>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <GuestRoutes exact path="/register">
          <Register />
        </GuestRoutes>
        <GuestRoutes exact path="/login">
          <Login />
        </GuestRoutes>
        <AuthRoutes exact path="/dashboard">
          <Dashboard />
        </AuthRoutes>
        <AuthRoutes exact path="/fund-account">
          <FundAccount />
        </AuthRoutes>
        <AuthRoutes exact path="/withdraw-funds">
          <WithdrawFunds />
        </AuthRoutes>
        <AuthRoutes exact path="/transfer-funds">
          <TransferFunds />
        </AuthRoutes>
        <Route path="*">
          <PageNotFound />
        </Route>
      </Switch>
    </Suspense>
  );
}

export default App;

const AuthRoutes = ({ children, ...other }) => {
  const history = useHistory();

  useEffect(() => {
    if (!localStorage.getItem("token")) {
      history.replace("/login");
    }
  });
  return <Route {...other}>{children}</Route>;
};

const GuestRoutes = ({ children, ...other }) => {
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem("token")) {
      history.replace("/dashboard");
    }
  });
  return <Route {...other}>{children}</Route>;
};
